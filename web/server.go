package web

import (
	"net/http"
	"strings"

	"gitee.com/qhitc_admin/grpcwebserver/grpcweb"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

type WebServer struct {
	GrpcWebServer *grpcweb.GrpcWebServer
	Router        *gin.Engine
	h2Handler     http.Handler
}

func NewServer(grpcWebServer *grpcweb.GrpcWebServer) *WebServer {
	//gin.SetMode(gin.ReleaseMode)
	return &WebServer{
		GrpcWebServer: grpcWebServer,
		Router:        gin.New(), // 初始化一个空Gin路由
	}
}

func (s *WebServer) Serve(addr string) error {

	// 监听端口并处理服务分流
	s.h2Handler = h2c.NewHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if s.GrpcWebServer.WrappedGrpc.IsGrpcWebRequest(r) ||
			r.Method == http.MethodOptions {
			// 判断协议是否为grpc-web
			s.GrpcWebServer.WrappedGrpc.ServeHTTP(w, r)
		} else if r.ProtoMajor == 2 &&
			strings.HasPrefix(r.Header.Get("Content-Type"), "application/grpc") {
			// 判断协议是否为http/2 && 是grpc
			s.GrpcWebServer.GrpcServer.ServeHTTP(w, r)
		} else {
			// 当作普通http api
			s.Router.ServeHTTP(w, r)
		}
	}), &http2.Server{})

	if err := http.ListenAndServe(addr, s.h2Handler); err != nil {
		log.Println("http server done:", err.Error())
		return err
	}
	return nil
}
