package grpcservice

import (
	api "gitee.com/qhitc_admin/grpcwebserver/grpcservice/webappapi"
	"gitee.com/qhitc_admin/grpcwebserver/jwthelper"

	"context"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Config interface {
	ReadJwtSecret() (secret string, err error)
	GetUser() (userName string, passWd string, err error)
}

type GrpcService struct {
	C Config

	//内部变量
	jwtSecret string

	//服务实现接口
	api.UnimplementedWebappServiceServer
}

const (
	SECRETDEFAULT = "secret"
)

// New returns a Service to be registered at a grpc.Server
func NewService(c Config) *GrpcService {
	svr := GrpcService{
		C: c,
	}
	svr.LoadJwtSecret()
	return &svr
}

func (s *GrpcService) LoadJwtSecret() {
	secret, err := s.C.ReadJwtSecret()
	if err == nil {
		s.jwtSecret = secret
	} else {
		s.jwtSecret = SECRETDEFAULT
	}
}
func (s *GrpcService) RegisterServers(sreg grpc.ServiceRegistrar) {
	api.RegisterWebappServiceServer(sreg, s)
}

// 一元拦截器
func (s *GrpcService) Unary() grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler) (interface{}, error) {
		log.Debugln("---------> the unaryServerInterceptor: ", info.FullMethod)
		if info.FullMethod != "/webappapi.WebappService/Login" {
			claims, err := jwthelper.GetClaimsFromContext(s.jwtSecret, ctx)
			if err != nil {
				//return nil, err
				status.Errorf(codes.Unauthenticated, "authentication valid fail, ", err.Error())
			}
			if claims == nil {
				return nil, status.Errorf(codes.Unauthenticated, "authentication fail, ", err.Error())
			}
		}
		return handler(ctx, req)
	}
}

// stream拦截器
func (s *GrpcService) Stream() grpc.StreamServerInterceptor {
	return func(
		srv interface{},
		stream grpc.ServerStream,
		info *grpc.StreamServerInfo,
		handler grpc.StreamHandler) error {
		log.Debugln("--------->the streamServerInterceptor: ", info.FullMethod)
		/*
			err := interceptor.authorize(stream.Context(), info.FullMethod)
			if err != nil {
				return err
			}
		*/

		claims, err := jwthelper.GetClaimsFromContext(s.jwtSecret, stream.Context())
		if err != nil {
			return err
		}
		if claims == nil {
			return status.Errorf(codes.Unauthenticated, "authentication fail, ", err.Error())
		}
		return handler(srv, stream)
	}
}

func (s *GrpcService) Echo(ctx context.Context, in *api.StringMessage) (*api.StringMessage, error) {

	return &api.StringMessage{Value: in.Value}, nil
}
