package grpcservice

import (
	api "gitee.com/qhitc_admin/grpcwebserver/grpcservice/webappapi"
	"gitee.com/qhitc_admin/grpcwebserver/jwthelper"

	"context"
	"crypto/md5"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *GrpcService) Login(ctx context.Context, in *api.LoginReqMessage) (*api.LoginRespMessage, error) {

	username, passwd, _ := s.C.GetUser()
	md5pass := fmt.Sprintf("%x", md5.Sum([]byte(passwd)))

	if in.Passwdmd5 == md5pass && in.Username == username {
		scr, err := jwthelper.CreateJWT(s.jwtSecret, in.Username, username, time.Now().Add(time.Hour*24).Unix())
		if err != nil {
			log.Errorln("Gen scr error")
		}
		log.Debugln("scr:", scr)
		return &api.LoginRespMessage{Token: scr}, nil
	}

	log.Debugln("Received: %v   %v", in.Username, in.Passwdmd5)
	return &api.LoginRespMessage{Token: ""}, status.Errorf(codes.Unauthenticated, "authentication fail")
}
