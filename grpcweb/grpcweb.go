package grpcweb

import (
	"google.golang.org/grpc"

	"github.com/improbable-eng/grpc-web/go/grpcweb"
	log "github.com/sirupsen/logrus"
)

type GrpcWebServer struct {
	StreamInterceptor grpc.StreamServerInterceptor
	UnaryInterceptor  grpc.UnaryServerInterceptor
	AllowedOrigins    []string
	AllowAllOrigins   bool

	WrappedGrpc *grpcweb.WrappedGrpcServer
	GrpcServer  *grpc.Server
}

// Serve start GrpcServer,GrpcWebServer with defined options
func (server *GrpcWebServer) Serve() error {
	log.Debugln("Starting grpc server...")
	var opts []grpc.ServerOption

	if server.StreamInterceptor != nil {
		opts = append(opts, grpc.StreamInterceptor(server.StreamInterceptor))
	}

	if server.UnaryInterceptor != nil {
		opts = append(opts, grpc.UnaryInterceptor(server.UnaryInterceptor))
	}

	server.GrpcServer = grpc.NewServer(opts...)

	allowedOrigins := makeAllowedOrigins(server.AllowedOrigins)
	options := []grpcweb.Option{
		grpcweb.WithCorsForRegisteredEndpointsOnly(false),
		grpcweb.WithOriginFunc(server.makeHttpOriginFunc(allowedOrigins)),
	}
	server.WrappedGrpc = grpcweb.WrapServer(server.GrpcServer, options...)

	return nil
}

func makeAllowedOrigins(origins []string) *allowedOrigins {
	o := map[string]struct{}{}
	for _, allowedOrigin := range origins {
		o[allowedOrigin] = struct{}{}
	}
	return &allowedOrigins{
		origins: o,
	}
}
func (server *GrpcWebServer) makeHttpOriginFunc(allowedOrigins *allowedOrigins) func(origin string) bool {
	if server.AllowAllOrigins {
		return func(origin string) bool {
			return true
		}
	}
	return allowedOrigins.IsAllowed
}

type allowedOrigins struct {
	origins map[string]struct{}
}

func (a *allowedOrigins) IsAllowed(origin string) bool {
	_, ok := a.origins[origin]
	return ok
}
