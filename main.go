package main

import (
	"fmt"

	"gitee.com/qhitc_admin/grpcwebserver/config"
	"gitee.com/qhitc_admin/grpcwebserver/grpcweb"
	"gitee.com/qhitc_admin/grpcwebserver/web"

	"gitee.com/qhitc_admin/grpcwebserver/grpcservice"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

var (
// command-line options:

)

func main() {
	log.Println("hello world!")

	grpcService := grpcservice.NewService(config.Get())
	grpcWebServer := grpcweb.GrpcWebServer{
		StreamInterceptor: grpcService.Stream(),
		UnaryInterceptor:  grpcService.Unary(),
		AllowAllOrigins:   true,
	}
	grpcWebServer.Serve()
	grpcService.RegisterServers(grpcWebServer.GrpcServer)

	// 初始化一个空Gin路由
	webServer := web.NewServer(&grpcWebServer)
	go func() {
		if err := webServer.Serve(":8081"); err != nil {
			log.Errorln(err)
		}
	}()

	log.Println("web started..")

	//router := gin.New()
	/***** 添加你的api路由吧 *****/
	webServer.Router.Any("/hello", func(ctx *gin.Context) {
		fmt.Println("已进入到HTTP实现")
		ctx.JSON(200, map[string]interface{}{
			"code": 200,
			"url":  ctx.Request.URL,
			"msg:": "success",
		})
	})
	webServer.Router.Any("/", func(ctx *gin.Context) {
		fmt.Println("已进入到HTTP实现")
		ctx.JSON(200, map[string]interface{}{
			"code": 200,
			"url":  ctx.Request.URL,
			"msg:": "success",
		})
	})
	select {}

}
