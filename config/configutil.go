package config

import (

	//"fmt"
	"errors"
	"reflect"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func viperBindEnvs(iface interface{}, parts ...string) {
	ifv := reflect.ValueOf(iface)
	ift := reflect.TypeOf(iface)
	for i := 0; i < ift.NumField(); i++ {
		v := ifv.Field(i)
		t := ift.Field(i)
		tv, ok := t.Tag.Lookup("mapstructure")
		if !ok {
			tv = strings.ToLower(t.Name)
		}
		if tv == "-" {
			continue
		}

		switch v.Kind() {
		case reflect.Struct:
			viperBindEnvs(v.Interface(), append(parts, tv)...)
		default:
			// Bash doesn't allow env variable names with a dot so
			// bind the double underscore version.
			keyDot := strings.Join(append(parts, tv), ".")
			keyUnderscore := strings.Join(append(parts, tv), "__")
			viper.BindEnv(keyDot, strings.ToUpper(keyUnderscore))
		}
	}
}

func Loadsetting() {
	viper.SetConfigType("toml")
	viper.SetConfigName("webapp")
	viper.AddConfigPath(".")
	viper.AddConfigPath("$HOME/.config/webapp")
	viper.AddConfigPath("/etc/webapp")
	viper.SetDefault("general.log_level", 4)
	viper.SetDefault("general.dsn", "postgres://postgres:490312@localhost:5432/alarmserver")
	viper.SetDefault("general.ttspath", "z:/tts/")
	viper.SetDefault("general.ttsapikey", "wrvYjmUCLtGTE6jCi6LxGhV5")
	viper.SetDefault("general.ttssecretkey", "4pDI7y60Ljl5mRU1XAe8FoP9UdhaFDq6")
	viper.SetDefault("general.updatecheckurl", "https://gitee.com/qhitc_admin/distributed-alarm-system/releases/download/V1.0/alarmServer")

	viper.SetDefault("external_api.bind", ":80")
	viper.SetDefault("external_api.passwd", "1234567")
	viper.SetDefault("external_api.secret", "secret")
	viper.SetDefault("external_api.user", "admin")

	viper.SetDefault("controller_proxy.ip", "")
	viper.SetDefault("controller_proxy.port", 7777)
	viper.SetDefault("controller_proxy.token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjcyNTgwODk2MDAsImlhdCI6MzE1NTA0MDAwLCJpc3MiOiJncmVldGVyIiwidXNlcm5hbWUiOiJhZG1pbiIsInJvbGVzIjoiYWRtaW4ifQ.YekQ34bX7wL-eKiLZnaYbHhMmJMx_6rerPqgBWSrZds")
	viper.SetDefault("controller_proxy.service_path", "/controllerapi.ContrlService/")

	viper.SetDefault("comserver_proxy.ip", "")
	viper.SetDefault("comserver_proxy.port", 7778)
	viper.SetDefault("comserver_proxy.token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjcyNTgwODk2MDAsImlhdCI6MzE1NTA0MDAwLCJpc3MiOiJncmVldGVyIiwidXNlcm5hbWUiOiJhZG1pbiIsInJvbGVzIjoiYWRtaW4ifQ.YekQ34bX7wL-eKiLZnaYbHhMmJMx_6rerPqgBWSrZds")
	viper.SetDefault("comserver_proxy.service_path", "/api.ComServerService/")

	noconf := false
	if err := viper.ReadInConfig(); err != nil {
		switch err.(type) {
		case viper.ConfigFileNotFoundError:
			log.Warning("No configuration file found, using defaults. See: ...")
			noconf = true
		default:
			log.WithError(err).Fatal("read configuration file error")
		}
	}

	viperBindEnvs(Conf)

	if err := viper.Unmarshal(&Conf); err != nil {
		log.WithError(err).Fatal("unmarshal config error")
		return
	}
	if noconf {
		err := viper.WriteConfigAs("webapp.toml")
		if err != nil {
			log.Debugln(err.Error())
		}
	}

}

func SaveNetworkSetting(ip string, mask string, gateway string, tbserver string) error {
	viper.Set("external_api.netip", ip)
	viper.Set("external_api.netmask", mask)
	viper.Set("external_api.netgateway", gateway)
	viper.Set("tbmqtt.server", tbserver)
	err := viper.WriteConfig()
	if err != nil {
		log.Debugln(err.Error())
	}
	return err
}

func SavePasswd(passwd string) error {
	viper.Set("external_api.passwd", passwd)
	err := viper.WriteConfig()
	if err != nil {
		log.Debugln(err.Error())
	}
	return err
}

func (c *Config) ReadJwtSecret() (secret string, err error) {
	if c.ExternalAPI.Secret != "" {
		return c.ExternalAPI.Secret, nil
	} else {
		return "", errors.New("secret not config")
	}
}

func (c *Config) GetUser() (userName string, passWd string, err error) {
	return c.ExternalAPI.User, c.ExternalAPI.Passwd, nil
}
