package config

type Config struct {
	General struct {
		LogLevel                  int    `mapstructure:"log_level"`
		LogToSyslog               bool   `mapstructure:"log_to_syslog"`
		PasswordHashIterations    int    `mapstructure:"password_hash_iterations"`
		GRPCDefaultResolverScheme string `mapstructure:"grpc_default_resolver_scheme"`
		DSN                       string `mapstructure:"dsn"`
		TtsPath                   string `mapstructure:"ttspath"`
		TtsApiKey                 string `mapstructure:"ttsapikey"`
		TtsSecretKey              string `mapstructure:"ttssecretkey"`
		UpdateCheckURL            string `mapstructure:"updatecheckurl"`
	} `mapstructure:"general"`
	ExternalAPI struct {
		Bind       string `mapstructure:"bind"`
		Secret     string `mapstructure:"secret"`
		User       string `mapstructure:"user"`
		Passwd     string `mapstructure:"passwd"`
		NetIp      string `mapstructure:"netip"`
		NetMask    string `mapstructure:"netmask"`
		NetGateway string `mapstructure:"netgateway"`
		UdpPort    uint32 `mapstructure:"udpport"`
	} `mapstructure:"external_api"`
	ControllerProxy struct {
		Ip          string `mapstructure:"ip"`
		Port        int32  `mapstructure:"port"`
		Token       string `mapstructure:"token"`
		ServicePath string `mapstructure:"service_path"`
	} `mapstructure:"controller_proxy"`
	ComserverProxy struct {
		Ip          string `mapstructure:"ip"`
		Port        int32  `mapstructure:"port"`
		Token       string `mapstructure:"token"`
		ServicePath string `mapstructure:"service_path"`
	} `mapstructure:"comserver_proxy"`
}

// C holds the global configuration.
var Conf Config

// Get returns the configuration.
func Get() *Config {
	return &Conf
}

// Set sets the configuration.
func Set(c Config) {
	Conf = c
}
